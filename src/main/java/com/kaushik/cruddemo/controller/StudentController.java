package com.kaushik.cruddemo.controller;

import com.kaushik.cruddemo.entity.Student;
import com.kaushik.cruddemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class StudentController {

    private StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService){
        this.studentService=studentService;
    }

    @PostMapping("/saveStudent")
    public Student createStudent(@RequestBody Student student){
        studentService.saveStudent(student);
        return student;
    }

    @GetMapping("/students")
    public List<Student> getStudents(){
        return studentService.getStudents();
    }

    @GetMapping("/student/{id}")
    public Student getStudent(@PathVariable("id") int id){
        Student student=studentService.getStudentById(id);
        if(student ==null) {
            throw new RuntimeException("Student id not found - "+id);
        }
        return student;
    }

    @PutMapping("/student")
    public Student updateStudent(@RequestBody Student student){
        studentService.updateStudent(student);
        return student;
    }

    @DeleteMapping("/student/{id}")
    public String deleteStudent(@PathVariable("id") int id){
       try {
           studentService.deleteStudentById(id);
       }catch(Exception exc){
           throw new RuntimeException("Student can't be deleted for id :: " + id);
       }
       return "Successfully deleted student of roll no :"+id;
    }
}