package com.kaushik.cruddemo.repository;

import com.kaushik.cruddemo.entity.Student;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface StudentRepository extends CrudRepository<Student,Integer> {

    @Transactional
    @Modifying
    @Query("update Student s set s.rollNo = :rollNo , s.name = :name , s.standard = :standard WHERE s.rollNo = :rollNo")
    void updateStudentById(@Param("rollNo") int rollNo,@Param("name") String name,@Param("standard") String standard);
}