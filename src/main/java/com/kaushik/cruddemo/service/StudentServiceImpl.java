package com.kaushik.cruddemo.service;

import com.kaushik.cruddemo.entity.Student;
import com.kaushik.cruddemo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService{

    private StudentRepository studentRepository;

    @Autowired
    public StudentServiceImpl(StudentRepository studentRepository){
        this.studentRepository=studentRepository;
    }

    @Override
    public void saveStudent(Student student) {
        studentRepository.save(student);
    }

    @Override
    public List<Student> getStudents() {
        return (List<Student>) studentRepository.findAll();
    }

    @Override
    public Student getStudentById(int id) {
        Optional<Student> studentOptional=studentRepository.findById(id);
        Student student=null;
        if(studentOptional.isPresent()){
            student=studentOptional.get();
        }else{
            throw new RuntimeException("Student not found for id :: " + id);
        }
        return student;
    }

    @Override
    public void updateStudent(Student student) {
        try {
            studentRepository.updateStudentById(student.getRollNo(),
                    student.getName(),student.getStandard());
        }catch (Exception e){
            throw new RuntimeException("Student is not Updated");
        }
    }

    @Override
    public void deleteStudentById(int id) {
        try {
            studentRepository.deleteById(id);
        }catch (Exception e){
            throw new RuntimeException("Student is not deleted");
        }
    }
}
