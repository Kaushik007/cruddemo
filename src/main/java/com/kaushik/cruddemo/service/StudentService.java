package com.kaushik.cruddemo.service;

import com.kaushik.cruddemo.entity.Student;

import java.util.List;
import java.util.Optional;

public interface StudentService {
    void saveStudent(Student student);

    List<Student> getStudents();

    Student getStudentById(int id);

    void updateStudent(Student student);

    void deleteStudentById(int id);
}
