package com.kaushik.cruddemo;

import com.kaushik.cruddemo.entity.Student;
import com.kaushik.cruddemo.repository.StudentRepository;
import com.kaushik.cruddemo.service.StudentServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class StudentServiceTest {

    private StudentServiceImpl studentService;

    @Mock
    private StudentRepository studentRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void addStudent() {
        studentService = new StudentServiceImpl(studentRepository);
        Student inputStudent = new Student("Shiv Kumar",  "ten");
        Student outputStudent = new Student("Shiv Kumar",  "ten");
        studentService.saveStudent(inputStudent);
        ArgumentCaptor<Student> studentArgumentCaptor = ArgumentCaptor.forClass(Student.class);
        verify(studentRepository).save(studentArgumentCaptor.capture());
        Student capturedUser = studentArgumentCaptor.getValue();
        assertEquals(outputStudent.getName(), capturedUser.getName());
    }

    @Test
    public void getStudentById() {
        studentService = new StudentServiceImpl(studentRepository);
        Student student = new Student( "Shiv Kumar","ten");
        student.setRollNo(1);
        studentRepository.save(student);
        when(studentRepository.findById(1)).thenReturn(java.util.Optional.of(student));
        assertEquals("Shiv Kumar", studentService.getStudentById(1).getName());
    }

    @Test
    public void getAllStudents() {
        studentService = new StudentServiceImpl(studentRepository);
        Student student = new Student("Shiv Kumar", "ten");
        studentRepository.save(student);
        List<Student> students = new ArrayList<>();
        students.add(student);
        when(studentRepository.findAll()).thenReturn(students);
        assertEquals(1, studentService.getStudents().size());
    }
}